import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IProduct } from './product';
import { ProductService } from './product.service'

import 'rxjs/add/operator/map';

@Component({
    templateUrl: 'app/products/product-edit.component.html'
})
export class ProductEditComponent implements OnInit {
    pageTitle: string = 'Product Edition';
    product: IProduct;
    errorMessage: string;
    submitted: boolean = false;

    constructor(private _route: ActivatedRoute,
                private _router: Router,
                private _productService: ProductService) {
    }

    ngOnInit(): void {
        let id = +this._route.snapshot.params['id'];
        this.pageTitle += `: ${id}`;
        
        this._productService.getProductById(id)
            .subscribe(
                product => this.product = product,
                error => this.errorMessage = <any>error
            );
    }

    onSubmit(): void {
        this.submitted = true;
    }

    onContinue(): void {
        this._router.navigate(['/products']);
    }
    // TODO: Remove this when we're done
    get diagnostic() { return JSON.stringify(this.product); }
}
