import { Component, Input, Output, EventEmitter } from '@angular/core'; 

@Component({
    selector: 'nat-star',
    templateUrl: 'app/shared/star.component.html',
    styleUrls: ['app/shared/star.component..css']
})

export class StarComponent {
    public cssStyle: string = 'width: ' + this.starWidth + 'px';
    public starWidth: number;
    @Input() rating: number;
    @Output() ratingClicked: EventEmitter<string> =
        new EventEmitter<string>();
    onClick() { this.notify.emit('clicked');
}