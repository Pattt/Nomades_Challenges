import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { IProduct } from './product';
import { ProductFilterPipe } from './product-filter.pipe';
import { StarComponent } from '../shared/star.component';

@Component({
    selector: 'nat-products',
    templateUrl: 'app/products/product-list.component.html',
    styleUrls: ['app/products/product-list.component.css']
})

export class ProductListComponent implements OnInit {
    pageTitle: string = 'Product List';
    showImage: boolean = true;
    imageSize: number = 70;
    saisieText: string;
    listFilter: string;
    products: Array<IProduct> = [
        {
            "productId": 2,
            "productName": "Garden Cart",
            "productCode": "GDN-0023",
            "releaseDate": "March 18, 2016",
            "description": "15 gallon capacity rolling garden cart",
            "price": 32.99,
            "starRating": 4.2,
            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
        },
        {
            "productId": 5,
            "productName": "Hammer",
            "productCode": "TBX-0048",
            "releaseDate": "May 21, 2016",
            "description": "Curved claw steel hammer",
            "price": 8.9,
            "starRating": 4.8,
            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
        }
    ];
    ngOnInit() {
        console.log('Component Initiated ---');
        
    }
    public toggleImage() {
        this.showImage = !this.showImage;
    }
    public getImageTextBtn() : string {
        let text = 'show';
        if(this.showImage) {
            text = 'hide';
        }
        return text;
    }

    public onNotify(message: string): void {}
}
