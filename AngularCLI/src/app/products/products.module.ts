import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';

import { SharedModule } from '../shared/shared.module';

import { ProductsComponent } from './products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductFilterPipe } from './product-filter.pipe';
import { ProductService } from './product.service';
import { ProductGuardService } from './product-guard.service';

@NgModule({
  declarations: [
    ProductsComponent, 
    ProductDetailComponent,
    ProductFilterPipe
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
            { path: 'products', component: ProductsComponent },
            { path: 'product/:id',
              canActivate: [ ProductGuardService ],
              component: ProductDetailComponent },
        ])
  ],
  providers: [
        ProductService,
        ProductGuardService
    ]
})
export class ProductsModule { }
