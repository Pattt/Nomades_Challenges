import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { IProduct } from './product';
import { ProductFilterPipe } from './product-filter.pipe';
import { ProductService } from './product.service';

@Component({
    selector: 'nat-products',
    templateUrl: 'app/products/product-list.component.html',
    styleUrls: ['app/products/product-list.component.css'],
    providers: [ProductService]
})

export class ProductListComponent implements OnInit {
    private _productService: ProductService;

    pageTitle: string = 'Product List';
    showImage: boolean = true;
    imageSize: number = 70;
    saisieText: string;
    listFilter: string;
    products: Array<IProduct> = null;
    
    constructor(productService: ProductService) {
        this._productService = productService;
    }

    public ngOnInit() {
        console.log('Component Initiated ---');
        debugger;
        this.products = this._productService.getProducts();
    }

    public toggleImage() {
        this.showImage = !this.showImage;
    }

    public getImageTextBtn() : string {
        let text = 'show';
        if(this.showImage) {
            text = 'hide';
        }
        return text;
    }

    public onRatingClicked(message: string): void {
        this.pageTitle = 'Product List: ' + message;
    }
}
