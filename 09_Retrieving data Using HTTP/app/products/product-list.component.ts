import { Component, OnInit } from '@angular/core';

import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
    selector: 'nat-products',
    templateUrl: 'app/products/product-list.component.html',
    styleUrls: ['app/products/product-list.component.css']
})

export class ProductListComponent implements OnInit {
    pageTitle: string = 'Product List';
    showImage: boolean = true;
    imageSize: number = 70;
    saisieText: string;
    listFilter: string;
    errorMessage: string;
    products: Array<IProduct> = null;
    
    constructor(private _productService: ProductService) {}

    public ngOnInit(): void {
        this._productService.getProducts()
            .subscribe(
                retreivedProducts => { 
                    this.products = retreivedProducts
                },
                error => this.errorMessage = <any>error
            );
    }

    public toggleImage() {
        this.showImage = !this.showImage;
    }

    public getImageTextBtn() : string {
        let text = 'show';
        if(this.showImage) {
            text = 'hide';
        }
        return text;
    }

    public onRatingClicked(message: string): void {
        this.pageTitle = 'Product List: ' + message;
    }
}
