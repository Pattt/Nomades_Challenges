import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core'

@Component({
    selector: 'nat-star',
    templateUrl: 'app/shared/star.component.html',
    styleUrls: ['app/shared/star.component.css']
})

export class StarComponent implements OnChanges{
    public cssStyle: string = 'width: ' + this.starWidth + 'px';
    public starWidth: number;
    @Input() rating: number;
    @Output() ratingClicked: EventEmitter<string> =
        new EventEmitter<string>();

    onClick(): void {
        this.ratingClicked.emit(`The rating ${this.rating} was clicked!`);
    }
    
    ngOnChanges(): void {
        this.starWidth = this.rating * 86 / 5;
    }
}